# P1 cookie telegram parser

Service for listening on the MQTT bus for P1 cookie (digital utility meter) messages, parsing the contained metering telegram, processing it and sending it to the standardised COFY MQTT endpoints.
If requested, the service also registers the device with HA.
Configuration is done through configuration files which must be placed in the same directory as this file.
This code is meant to be run continously as a Unix service. See the dryrun version for testing.
