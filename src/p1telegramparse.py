# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 22:37:55 2020

Service for listening on the MQTT bus for P1 cookie (digital utility meter) messages,
parsing the contained metering telegram, processing it and sending it to the standardised COFY MQTT endpoints.
If requested, the service also registers the device with HA.
Configuration is done through configuration files which must be placed in the same directory as this file.
This code is meant to be run as a Unix proces. See the dryrun version for testing.

@author: Joannes //Epyon// Laveyne
"""
import json
import logging
import os
import signal
from datetime import datetime

import ha_autodiscovery
import paho.mqtt.client as mqtt
import yaml
from dateutil import tz

_LOGGER = logging.getLogger(__name__)

dir_path = os.path.dirname(os.path.realpath(__file__))
_PRODUCTION_ = True
_HAdiscovery_ = True
response = []

"""
MQTT configuration vars
Could be moved to config file if needed
"""
client = mqtt.Client("utility_meter")
brokers_out = {"broker1": "mosquitto"}

"""
When the Grim Sysadmin comes to reap with his scythe,
let this venerable daemon process die a Graceful Death
"""


class GracefulDeath:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


"""
Utility function to check if meter value is numeric or string
"""


def is_number(s):
    try:
        r = float(s)
        return (True, r)
    except ValueError:
        try:
            r = int(s)
            return (True, r)
        except ValueError:
            return (False, s)


"""
Read in the config file containing the cookie configruation
and OBIS keys requested to be extracted from the meter telegram
Make sure the p1config.yaml file is placed in the same folder
"""
keys = {}
timestamp = 0
subs = []
with open(dir_path + "/" + "p1config.yaml", "r") as stream:
    try:
        config = yaml.safe_load(stream)
        cookie = config["configuration"]
        subs = [("stat/" + cookie["cookie"]["id"] + "/p1/telegram", 0)]
        keys = config["keys"]
    except yaml.YAMLError as exc:
        _LOGGER.exception(exc)
client.connect(brokers_out["broker1"])
client.subscribe(subs)

"""
On receiving the MQTT payload, decode it and pass it to the telegram parser
Set the timestamp according to the payload timestamp
Perhaps add integrity check here later
"""


def on_telegram(mosq, obj, msg):
    try:
        data = json.loads(msg.payload.decode())
        telegram = data["value"].encode().decode("unicode_escape")
        global timestamp
        timestamp = data["timestamp"]
        parseMetertelegram(telegram)
    except Exception as e:
        _LOGGER.error(e)
        _LOGGER.error(f"Error parsing telegram {msg.payload.decode()}")


"""
Parse the OBIS timestamp
"""


def parseMeterTime(mtimestamp):
    dst = True
    # OBIS timestamps have a trailing character indicating DST or not
    # Currently unused, we convert the naive timestamp to UTC based on system locale
    if mtimestamp[-1:] == "W" or mtimestamp[-1:] == "S" or mtimestamp[-1:] == "Z":
        if mtimestamp[-1:] == "W":
            dst = False
        mtimestamp = mtimestamp[:-1]
    try:
        # Use the system locale to turn the datetime into epoch timestamp
        datetime_object = datetime.strptime(mtimestamp, "%y%m%d%H%M%S").astimezone(
            tz.UTC
        )
        mtimestamp = int(datetime_object.timestamp())
        return mtimestamp
    except:
        return timestamp


"""
Parse the telegram, extract requested keys and publish them to the relevant MQTT topics
"""


def parseMetertelegram(telegram):
    """
    Parse the raw meter telegram into its atomic OBIS key:value pairs
    """
    dlist = []
    for line in telegram.splitlines():
        if not line:
            continue  # Filter out blank lines
        splitline = line.split("(", 1)
        if len(splitline) > 1:
            splitline[1] = str("(") + splitline[1]
        else:
            if splitline[0][0] == "/":
                splitline = ["Header", splitline[0]]
            else:
                splitline = ["CRC", splitline[0]]
        dlist.append(splitline)
    meterdata = dict(dlist)
    """
    Check if the telegram contains the meter local timestamp (DSMR 4.0 or higher)
    Otherwise, use timestamp of message payload (P1 cookie local)
    """
    mtimestamp = timestamp
    if meterdata.get("0-0:1.0.0"):
        mtimestamp = parseMeterTime(meterdata.get("0-0:1.0.0").lstrip("(").rstrip(")"))
    """
    Check if the requested OBIS keys are present in the meter telegram,
    parse them, build the JSON response and send it to the correct MQTT topic
    """
    entity = "utility_meter"
    topic = "data/devices/" + entity + "/"
    for key in keys:
        entry = keys[key]
        rvalue = meterdata.get(entry.get("key"))
        # Only build the response if the requested key is present in the telegram
        if rvalue:
            result = {}
            result["entity"] = entity
            name = str(key)
            result["channel"] = name
            # OBIS values are (encapsulated)
            rvalue = rvalue.lstrip("(").rstrip(")")
            rvalue = rvalue.rstrip(")")
            # If no timestamp is requested to be extracted from the value,
            # do nothing and use the default timestamp
            # Else, (try to) extract the timestamp from the OBIS value
            # These timestamps are added by some external meters (gas, heat),
            # which are polled slower than the energy meter
            if (
                entry.get("timestamped") != "no"
                or entry.get("timestamped") is not False
            ):
                # Additional timestamps are also (encapsulated), try to split them off based on the separator
                if rvalue.count(")(") == 1:
                    value = rvalue.split(")(")[1]
                    mtimestamp = parseMeterTime(rvalue.split(")(")[0])
                    rvalue = value
            # If no unit of measurement is explicitly given, try to extract the unit from the OBIS value
            unit = None
            if (
                entry.get("unit_of_measurement") == "auto"
                or entry.get("unit_of_measurement") is None
            ):
                # If present, the unit is preceded by an * in the OBIS value
                if rvalue.count("*") == 1:
                    unit = rvalue.split("*")[1]
                    value = rvalue.split("*")[0]
                else:
                    value = rvalue
            else:
                unit = str(entry.get("unit_of_measurement"))
            # Check if the OBIS value is a numeric value before doing rescaling
            if is_number(value)[0] and entry.get("data_type") != "string":
                value = is_number(value)[1]
                # Perform value rescaling if requested
                if entry.get("multiplier") is not None:
                    if is_number(entry.get("multiplier"))[0]:
                        value = value * is_number(entry.get("multiplier"))[1]
                if entry.get("offset") is not None:
                    if is_number(entry.get("offset"))[0]:
                        value = value + is_number(entry.get("offset"))[0]
            result["value"] = value
            result["unit"] = unit
            if entry.get("friendly_name") is not None:
                fname = str(entry.get("friendly_name"))
                result["friendly_name"] = fname
            if entry.get("device_class") is not None:
                devclass = str(entry.get("device_class"))
                result["device_class"] = devclass
            result["timestamp"] = mtimestamp
            # Build the response for this OBIS key
            rtopic = topic + name
            resp = json.dumps(result)
            # If in production env, publish the response to the broker
            if _PRODUCTION_:
                try:
                    client.publish(rtopic, resp)
                    # client.disconnect()
                    _LOGGER.info(rtopic + ": " + resp)
                except:
                    break
            # Log and temporarily store the result for debugging
            _LOGGER.debug(rtopic + ": " + resp)
            response.append(result)
    global _HAdiscovery_
    if _HAdiscovery_ is True:
        try:
            ha_autodiscovery.register(response, _PRODUCTION_)
            _LOGGER.info("Succesfully registered device with Home Assistant")
            _HAdiscovery_ = False
        except:
            _LOGGER.error("Error registering device with Home Assistant")
            _HAdiscovery_ = False


"""
Main loop
Check for MQTT messages, on arrival dispatch them to the parser
Meant to keep running as a service, untill killed by UNIX kill command
"""
# Nooooo you can't just import me as a module in your program nooooo
if __name__ == "__main__":
    killer = GracefulDeath()
    while not killer.kill_now:
        try:
            client.loop_start()
            # Read in the telegram
            # client.message_callback_add('stat/COFYAC4/p1/telegram', on_telegram)
            client.message_callback_add(subs[0][0], on_telegram)
            client.loop_stop()
        except KeyboardInterrupt:
            _LOGGER.info("Unsubscribing from topics...")
            for topic in subs:
                client.unsubscribe(topic[0])
            _LOGGER.info("done")
            _LOGGER.info("Disconnecting from broker... ", end="")
            client.disconnect()
            _LOGGER.info("Press Ctrl-C to terminate while statement")
            pass
    # Make a graceful exit when this daemon gets terminated
    _LOGGER.info("Termination signal received")
    _LOGGER.info("Unsubscribing from topics... ", end="")
    for topic in subs:
        client.unsubscribe(topic[0])
    _LOGGER.info("done")
    _LOGGER.info("Disconnecting from broker... ", end="")
    client.disconnect()
    _LOGGER.info("done")
    _LOGGER.info("Goodbye, cruel Unix world")
