#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    python p1telegramparse.py
else
    echo "P1-cookie-parser block not enabled by environment variable"
    break
fi